\documentclass[a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[english]{babel}

\usepackage[margin=1in]{geometry}

\usepackage{multicol}
\usepackage{graphicx}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{enumitem}

\usepackage{amsmath}

\setlist{itemsep=0pt}
\floatplacement{figure}{H}
\newcommand{\HRule}{\rule{\linewidth}{0.2mm}}

\begin{document}

\title{\vspace{-18mm}\huge{Finding Centroids on Low Ram Computer Systems}\vspace{-2mm}}
\author{Geoffrey Irons (34591492) \\ \small{Supervised by Prof. Richard Green}\vspace{-2mm}}
\date{\today \\ \HRule{} \vspace{-10mm}}
\maketitle{}

\section{Introduction} 
Computer vision is a rapidly developing field in software driven by
increasing processor speeds, growing memory sizes and ever higher 
resolution cameras. Even though computer vision now offers capabilities 
exceeding regular sensing technologies, it's use in robotics has been 
hindered by the large cost and energy consumption of the computer 
system required. This project aims to research and develop
computer vision techniques for low ram situations: when there is not
enough ram to store the image. These techniques have the possibility
to allow a greater range of embedded vision products.

The major focus of this project is to identify the centroid of 
blocks of colour using as little RAM (random access memory) as 
possible. Finding the center of area of a coloured area allows robots 
to align themelves far more accurately than traditional sensors (as 
every pixel is effectively a sensor).

\section{Background}
\subsection{Filtering Kernels}
A basic operation for improving the results of an image processing 
algorithm involve filtering kernels. A filtering kernel takes a 3x3 or 
5x5 matrix, and adjusts the value of a pixel based on the values of 
surrounding pixels. Kernals can do operations such as blurring, 
sharpening and edge detect. Of interest to this project is blurring as 
it effectively reduces high frequency noise that adds noise to 
thresholding. \cite{gaussiansegmentation} estabished that while
a gaussian blur reduces the information contained in an image, it
is still useful for generating masks (ie finding blocks of similar
appearance)

Existing implementations of filtering kernals require the entire image
to be stored in RAM twice (one as an image to copy values from, the
other as an image to put values into).

\subsection{Segmentation}
In order to find the centroid, an area must be identified. This can be 
done by colour, texture and several other methods. For this project, 
the focus will mainly be on colour thresholding (as shown in Figure 
\ref{fig:threshold}) as this is the most basic form of segmentation. 
Thresholding is done by simple comparison to a constant, and can be
done easily on any component in a variety of colour-spaces.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{Images/threshold.jpg}
    \caption{Thresholding an image in YCbCr. From left to right: original image,
    luminosity, redness, blueness}
    \label{fig:threshold}
\end{figure}

\subsection{Finding Centroids}
Finding the centroid in a discrete system is nothing new and as such 
the methods used are widely understood. A typical forumlation, as 
found in \cite{imageprochandbook}, is:
\begin{center}
\begin{math}
    CG_{x} = \dfrac{\sum{Value_{i}} * x_{i}}{\sum{Value_{i}}}
\end{math}
\end{center}
Where $Value$ is the number of pixels meeting the criteria in the 
column and $x_{i}$ is the columns horizontal location. This gives only
the horizontal position of the centroid. The operations must be 
repeated swithing rows an columns to locate the vertical position.
There are two potential issues with this 
operation. The first is that the whole image needs to be stored to 
count the pixels in the column, and the other is that the summations 
may reach high values. An image in SCIF (256x192), with the width 
fitting neatly into a 8 bits, may require up to 6266880\footnote{Found 
using $(\displaystyle\sum_{i=1}^{256}i)*192$ as you have to sum each pixel $x$ 
position for every row} (requiring 32 bits) to be stored if all pixels 
fall into the selected thresholds.

\section{Proposed Solution}
The basic idea is to perform the above algorithms without ever storing
the image. Each pixel should be stored for the minimum
amount of time possible. This should remove the dependance of ram usage on
image size, meaning that even a small, cheap processor will be 
able to processing images (though perhaps slowly). As such, some
of the algorithms described in the previous section need modification.

\subsection{Filtering Kernels}
There are three ways of lowering the ram usage of a filtering kernel.
The first is to store only 3 rows of the image at a time, allowing
a perfect filtering kernel operation to be performed. This uses the
most ram of these alternate methods, but results in the same
output as if no ram-reduction was in use. The second method is to 
do on a one dimensional filtering of the image, resulting in the 
gaussian noise-reduction only happening on a single axis. This requires
only three pixels to be stored, but the pixel being operated on
is not the most recent one, meaning that co-ordination with
the rest of the script may be hard. The third 
and most efficient method is to do a one-dimensional filter with only 
previous values. Again this requires only three pixels to be stored, and
this time the pixel being operated in is the most recent. These
three methods are shown diagramatically in Figure \ref{fig:kernel}.

Experimentation will have to be undertaken to determine if a 
one-dimensional one-directional filter is effective at removing
high frequency noise from an image.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{Images/Filtering.jpg}
    \caption{Three ways of lowering ram usage of a filtering kernel. 
    (a) is to only store three rows, (b) is to do it only in one 
    dimension, and (c) is to only use past data in one dimension. The green square is the pixel being operated on}
    \label{fig:kernel}
\end{figure}

\subsection{Segmentation} 
Segmentation using thresholding is already done on a pixel-by-pixel 
basis, and thus needs little modification. The main difference between 
traditional processing is that instead of writing the result into a 
bit-map, it has to be passed directly into the centroid finding 
algorithm. 

\subsection{Finding Centroids}
Finding centroids using the formulae presented previously requires all
the pixels in a column to be counted. Since this is not possible with 
a streamed image, the formula has to be modified slightly. Instead
of doing the weighted overage of an entire column at a time, the result of
the threshold should be used as the value for each pixel, and each 
pixel (rather than each column) should be summed.
While the summations now contain a lot more elements, they do not require
the storage of all the pixels.

Since the expression for finding center of gravity of an area is 
essentially a weighted average, time has been put into experimenting 
with methods of averaging that don't require large numbers to be 
stored. One of particular interest is: (for each pixel)
\begin{center}
\begin{math}
\begin{array}{rcl}
    decay &=& \frac{1}{pixel\_number} \\ 
    average &=& new\_value * decay + average * (1 - decay)
\end{array}
\end{math}
\end{center}
However it only performs cumulative averages not cumulative weighted 
averages. More experimentaiton here is needed.


\section{Expected Results}
Directly thresholding an image and extracting the centroid is definitely
possible on a low-ram processor by never storing the image, but the 
quality of filtering can be is untested. Additionally, it should be 
possible to do weighted cumulative averages without storing large 
numbers, but a method for doing so has not yet been
determined.
\begin{thebibliography}{9}

\bibitem{gaussiansegmentation}
  Estevão S. Gedraite, Murielle Hadad. Investigation on the Effect of a Gaussian Blur in
Image Filtering and Segmentation. University of São Paulo, 2011.

\bibitem{imageprochandbook}
  Russ, John C. The Image Processing Handbook. Boca Raton, FL: CRC Press, 2011. Print.


\end{thebibliography}


\end{document}
