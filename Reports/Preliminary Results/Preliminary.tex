\documentclass[a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[english]{babel}

\usepackage[margin=1in]{geometry}

\usepackage{multicol}
\usepackage{graphicx}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage[titletoc]{appendix}

\usepackage{listings}
\usepackage{xcolor}
\lstset { %
    language=C++,
    backgroundcolor=\color{black!5}, % set backgroundcolor
    basicstyle=\footnotesize,% basic font setting
}


\usepackage{amsmath}

\setlist{itemsep=0pt}
\floatplacement{figure}{H}
\newcommand{\HRule}{\rule{\linewidth}{0.2mm}}

\begin{document}

\title{\vspace{-18mm}\huge{Finding Centroids on Low Ram Embedded Systems}\vspace{-2mm}}
\author{Geoffrey Irons (34591492) \\ \small{Supervised by Prof. Richard Green}\vspace{-2mm}}
\date{\today \\ \HRule{} \vspace{-10mm}}
\maketitle{}

\section{Summary of Project}
The aim of this project is to explore computer vision algorithms on low
resource hardware. In specific, the task of finding centroids of coloured
areas for when there is not enough system ram to store the image.
As such, the image has to be processed in a stream.

\section{Implementation and Assumptions}
To avoid lengthy delays in prototyping on an embedded system, 
the bulk of the development was done on a normal linux computer.
It has been assumed that an existing library for the embedded system
exists allowing it to access the camera data. As such, the centroid
finding system expects only two functions:
\begin{itemize}
    \item \emph{void takePhoto(void);} This tells the camera to take a new photo
    \item \emph{getPixel(uint8\_t* Y, uint8\_t* Cr, uint8\_t* Cb);} This sets the values of the passed in integers to their respective colours.
\end{itemize}
It is assumed that the camera resolution is known, and that the camera
is set into YUV colour space (many cameras, such as the OV2640 can 
output in multiple colour spaces).

A common interface for cameras is to feed it's output into a FIFO 
buffer (First In First Out), where each byte can be called retrieved
exactly once in the order that the camera output them. From this
each pixel can be read (in order) at a later time by the 
microprocessor. The functions (ie getPixel) have been designed with 
this in mind, assuming that inside getPixel, several colour values are 
read and converted into the desired colourspace for thresholding.

For debugging, printf statements were used to display the centroid 
location and area. An ASCII representation of the image was also 
output, allowing the quality of the filtering to be accessed 
quantifiably.


\section{Test Images} 
A series of test images was generated in order to 
test the algorithms. These are 50x50px images with several shapes and 
noise levels. All images go between 25\% grey and 75\% grey, with the 
threshold for testing the algorithm set to 50\% grey. The test images 
are visible in Figure \ref{fig:testimages}. The later images have noise 
added to test the filtering. Image (e) has 50\% amplitude colour noise 
and image (f) has 100\% amplitude noise. For details on noise
generation, see Appendix \ref{app:noise}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{Images/testimages.jpg}
    \caption{The 6 test images in order of complexity}
    \label{fig:testimages}
\end{figure}

%It is worth noting a peculiar property of images. In a 50x50px image,
%the centre is not 25 across and 25 down but 25.5 across and 25.5 down.
%This is easily visualised on a 2px by 2px image, where the centre is
%not 1 px across and 1px down, rather on the line between the pixels.
%This is illustrated in Figure \ref{fig:2x2grid}
%
%\begin{figure}[H]
%    \centering
%    \includegraphics[width=0.1\textwidth]{Images/2x2grid.jpg}
%    \caption{A 2x2 grid has the center at (1.5, 1.5)}
%    \label{fig:2x2grid}
%\end{figure}




\section{Comparison of Filtering}
\subsection{No Filtering}
In short by thresholding each pixel in turn, the darker pixels were
identified. In the case of the noiseless images, this is as desired
and results in the centroid being placed in the predicted location.
On image (e) with 50\% amplitude noise there were 18 pixels whose
values were influenced by the noise, and on image (f) with 100\%{}
amplitude noise, a total of 291 Pixels were distorted.
Because the noise is evenly distributed and the center of the dark
area is in the center of the image, the noise did not move the centroid
significantly - but this is an artefact of the test images.

\subsection{Filtering/blurring the image before thresholding}
One method tested to reduce the impact of noise in images (e) and (f)
is to apply a single dimension filtering kernel to the image. This 
requires storing only three pixels.as opposed to an entire slice of the
image as would be required for a 2-dimensional filtering kernel

Two kernel shapes were tried. The first was a box profile, with the 
three stored pixels having equal weighting $[\frac{1}{3}, \frac{1}{3}, 
\frac{1}{3}]$. The other kernel was $[\frac{1}{4}, \frac{1}{2}, 
\frac{1}{4}]$, which is equivalent to a gaussian blur with a standard 
deviation of just over one. To compare this to a traditional 2d kernel, 
filtering was turned off an pre-blurred images were fed into the 
algorithm.

The data generated is tabulated in Table \ref{table:filtering}.
\begin{table}[H]
    \centering
    \begin{tabular}{| c | c | c | c | c | }
      \hline
      Image Source  & Filtering Type & No Filtering & 1D filtering & 2D filtering \\
      \hline
      (e)           & Linear (3)     & 12           & 3            & 7  \\
      (e)           & Gaussian (3)   & 12           & 0            & 3  \\
      \hline
      (f)           & Linear (3)     & 291          & 97           & 25 \\
      (f)           & Gaussian (3)   & 291          & 106          & 17 \\
      \hline
    \end{tabular}
    \caption{Number of pixels distorted by noise for various filtering techniques}
    \label{table:filtering}
\end{table} 
As can be seen, filtering in one-dimension resulted in a 
large reduction in the error-rate - dividing the number of 'noisy' 
pixels by three or more.

It appears that the 2D filtering performs worse on image (e)
but this is because the edges were rounded (a blur is a low-pass 
filter). This was not present in the single dimension filtering as the 
image is sampled only a single row at a time (And is therefore not 
influenced by rows above and below). This is why the 2D filtering 
performed worse on image (e). In the case with more noise (images (f)),
the 2D filtering is shown to be superior to the 1D filtering.

IF TIME PERMITS I WANT TO DO COMPARE A 5-PIXEL 1D FILTER TO THE 3-PIXEL
2D FILTER.

A visual comparison of the 1-dimensional results can be seen in 
Figure \ref{fig:filtering}.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{Images/filtering.jpg}
    \caption{Comparison of 1D filtering methods}
    \label{fig:filtering}
\end{figure}

\subsection{Filtering/blurring the image after thresholding}
Another idea for filtering is to apply the filtering to the boolean 
filtered values - in a similar way to cellula automata. Doing so 
requires a lot less computation and storage (one bit per pixel) but it 
had mixed results, often making holes in the object or adding extra 
bits on. If two or more sampled pixels were 'true' then so was the 
current pixel. If two or more sampled pixels were 'false' then the 
current pixel was 'false.'

This filtering had mixed results
In the case of thin lines such as image (b), the signal
was completely filtered out. Noisy image (e) had two erroneous pixels,
and noisy image (f) had 106.  

\section{Comparison of Centroid Finding Methods}
Two different centroid finding algorithms were tested, and it was
discovered there was no practical difference between them.

The first was a traditional cumulative moving average:
\begin{lstlisting}
float centroid_x = 0;
float centroid_y = 0;

void average_pixel(int x_pos, int y_pos, int area_so_far){
    centroid_x = (x_pos + centroid_x * (area_so_far - 1))/ (area_so_far);
    centroid_y = (y_pos + centroid_y * (area_so_far - 1))/ (area_so_far);
\end{lstlisting}

And the second is to use a propety of exponential decays:

\begin{lstlisting}
float centroid_x = 0;
float centroid_y = 0;

void average_pixel(int x_pos, int y_pos, int area_so_far){
    float alpha = 1.0/area_so_far;
    centroid_x = ((x_pos * alpha) + (centroid_x * (1 - alpha)));
    centroid_y = ((y_pos * alpha) + (centroid_y * (1 - alpha)));
\end{lstlisting}

The second method offers the advantage that at no point does the 
computation exceed the biggest value passed in ($x\_pos * alpha$ is 
never greater than $x\_pos$). This was intended to increase the 
scalability of the centroid finding - so that any number of pixels 
could be passed in and the $centroid\_x * (area\_so\_far - 1)$ would 
not grow without bound. As was realized during implementation, because 
the floats are using IEEE 754 representation, the precion of both 
methods suffer as the number of samples increases (the first because 
the numerator grows, the second because alpha shrinks).

As such, there is no practical difference between the two when using
floating points to represent the centroid location.

\section{Results on Photos}
As well as testing the system on the test images, several actual
photos were processed. The images were filtered using the gaussian blur 
1D kernel and the centroids of the area where blue and redness (Cr and 
Cb) exceed 60\% was found. The images were processed at 600x360px 
resolution.
The images can be seen in Figure \ref{fig:testphotos} with the centroids
indicated by the red and cyan croshairs.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{Images/testphotos.jpg}
    \caption{Four of the photos used for testing.}
    \label{fig:testphotos}
\end{figure}

Two things should be pointed out. 
\begin{enumerate}
    \item The red crosshair on the lower left 
image appears offset. Careful observation reveals
that the photographers finger was just in frame (top right) leading to 
a few red pixels.

    \item That the red and blue marker pens in the image in the lower 
    right was picked up and not the basketball was pure coincidence.
\end{enumerate}

\section{Ram Use}
Because the images are processed pixel by pixel rather than storing
the image as a whole, the ram usage is independant from the image size.
As a result, the ram usage is minimal.

Ignoring the ram needed inside functions, only the following needs
to be stored between pixel calculations:
\begin{lstlisting}
//To calculate centroid the following are required per centroid:
float centroid_x = 0;       //Centroids horizontal position (so far)
float centroid_y = 0;       //Centroids vertical position (so far)
uint16_t current_x = 0;     //Current Pixel's X location
uint16_t current_y = 0;     //Current Pixel's Y location
uint32_t centroid_area = 0; //Should be able to contain size_x * size_y

//To do a pre threshold blur filter the folliwing are required (for a 3px kernel):
uint8_t Y1 = 0;
uint8_t Cr1 = 0;
uint8_t Cb1 = 0;
uint8_t Y2 = 0;
uint8_t Cr2 = 0;
uint8_t Cb2 = 0;
uint8_t Y3 = 0;
uint8_t Cr3 = 0;
uint8_t Cb3 = 0;

//To do a post blur filter the following are required (for a 3px bitmask)
bool P1 = 0;
bool P2 = 0;
bool P3 = 0;
\end{lstlisting}

This comes to meerly 235 bits of data. Assuming the bools are stored 
inside a bitmask it comes to 240 bits (30 bytes).

Obviously this isn't the entire situation as various values are passed
in-between functions, and memory usage can hide itself in 
long mathermatical operations such as:
\begin{lstlisting}
centroid_x = (x_pos + centroid_x * (area_so_far - 1))/ (area_so_far);
\end{lstlisting}
Which requires  4 additional numbers (32 bit floats) to be stored.

NEED TO CHAT TO RICHARD LOB, STEVE WEDDELL, OR PHILL BONES ABOUT GOOD 
WAYS TO CALCULATE/ESTIMATE STACK/HEAP/RAM USAGE.

As a preliminary assumption, I'd guesstimate less than 1kb of ram.

\section{Preliminary Conclusions}
A system capable of detecting the center of coloured area's without
storing the image was developed. Such a system is nearly completely 
image-resolution independant and thus capable of being applied
to areas other than embedded systems. As the resolution of camera's 
increases finding methods to reduce the ram consumption will become
more important.

Filtering images in a single dimension also proved to be feasable,
though not as effective as a normal two dimensional methods.

\section{Further Development}
Before the end of this project I want to actually get it running on 
an embedded system, or at least something that can be demonstrated 
nicely.

I may also investigate things like:
\begin{itemize}
    \item Ambient colour subtraction.
    \item Investigate ways of handling multiple objects.
\end{itemize}

\newpage{}

\section*{Appendices}
\begin{appendices}
\appendix
\section{Noise Generation for test images} \label{app:noise}
The noise on the test images was generated using Gimp Image editor's 
'hurl' function set with 50\% randomization. From Gimps reference
manual:

"The Hurl filter changes each affected pixel to a random color, so it 
produces real random noise. All color channels, including an alpha 
channel (if it is present) are randomized. All possible values are 
assigned with the same probability. The original values are not taken 
into account. All or only some pixels in an active layer or selection 
are affected, the percentage of affected pixels is determined by the 
Randomization (\%) option."
INSERT PROPER REFERENCE.

The hurl was performed on a separate layer which was then overlayed on 
the image. Because the alpha channel is randomized, some of the original
image shows through. To achieve 50\% amplitude, the hurl layer is
simply set to 50\% opacity.
\end{appendices}
\end{document}
