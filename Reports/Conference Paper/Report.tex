\documentclass[conference]{IEEEtran}

% *** GRAPHICS RELATED PACKAGES ***
%
\ifCLASSINFOpdf
  \usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
  \graphicspath{{./Images/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  \DeclareGraphicsExtensions{.pdf,.jpg,.png}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.
  % \usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi
\hyphenation{op-tical net-works semi-conduc-tor}

\usepackage{listings}
\usepackage{xcolor}
\lstset { %
    language=C++,
    backgroundcolor=\color{black!5}, % set backgroundcolor
    basicstyle=\footnotesize,% basic font setting
}

\begin{document}
\title{Low Ram Centroid Location}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
    \author{\IEEEauthorblockN{Geoffrey Irons}
    \IEEEauthorblockA{Richard Green\\
    Canterbury University\\
    Christchurch, New Zealand\\
    Email: gdi19@uclive.ac.nz}\\
}

% make the title area
\maketitle

\begin{abstract}
This paper proposes a method for locating the position of centroids
of coloured areas where the system ram is insufficient to store the 
image. To overcome this limitation, the image was processed a single 
pixel at a time, with minimal multisampling.
Segmentation was performed in YUV colour space, and the 
centroid was calculated using a culmulative moving average. The image
was preprocessed by a one-dimensional filtering kernel. A 
one-dimensional filter was unable to reach the performance of a 
two-dimensional filter, but in the test images was able to reduce the 
effect of noise from 1.3\% incorrectly detected to 0.2\% - an 
improvement by a factor of six. 

The implementation of the algorithm uses under 200 bytes of data and is 
resolution independant - capable of processing high resolution images 
with no extra ram required.

\end{abstract}


\IEEEpeerreviewmaketitle



\section{Introduction}
Computer vision is a rapidly developing field in software driven by 
increasing processor speeds, growing memory sizes and ever higher 
resolution cameras. Even though computer vision offers capabilities 
exceeding regular sensing technologies\cite{bib:sense}, it’s use in robotics has been 
hindered by the large cost and energy consumption of the computer 
system required. This paper proposes a method to locate the 
centroid of blocks of colour using as little RAM (Random Access Memory) 
as possible. Identifying blocks of colour is a key element to many
computer vision algorithms, and in an emdedded system ram is often
scarse.

When a camera takes an image, it then transfers it byte by byte to the
microcontroller. Typically, the microcontroller stores the entire image 
in RAM where it can be accessed later. To reduce the RAM required, this 
paper examines algorithms for locating the centroid of an coloured area
where each pixel is processed as it is received.

\hfill gdi
 
\hfill \today{}

\section{Overview}
\subsection{Background}
Locating the centroid of a coloured area is a trivial problem in 
and has been studied and extended in many 
papers.\cite{bib:blob1}\cite{bib:blob2}
The first part of many image processing algorithms is to read the image 
into RAM. This can take significant quantities of memory, requiring 
$bytes\_ram\_use = image\_width*image\_height*bytes\_per\_pixel$. This 
number grows quickly, and even a small VGA (640x480) colour image 
contains nearly 1Mb of data. This is greater than the total capacity of 
microprocessors such as the ATmega1280\footnote{The Arduino Mega} (8KB 
SRAM 64KB Flash\cite{bib:arduino}), and many Cortex M3 series 
microcontrollers (512KB Flash \cite{bib:atmel}). To process images 
using such devices, external flash or EEPROM is added, which increases 
the product cost.

Once the image has been stored, a processing operation is performed on 
the entire image. For locating the centroid of an area this will 
typically include blurring the image, or thresholding it agains set 
values. This is shown in Figure \ref{fig:passes}. Processing an image 
using multiple filter passes requires the entire image to be stored in 
memory, and in many cases two image must be stored (the old image needs 
to be stored while a new image is generated from the old).

\begin{figure}[h]
    \centering
    \includegraphics[width=0.3\textwidth]{passes}
    \caption{Locating the centroid using filtering passes}
    \label{fig:passes}
\end{figure}

One method of using an old image without storing two copies is to use a 
sliding window architecture\cite{bib:sliding}. This is where only the
required rows of the image is stored. This 
technique reduces the ram required to only being dependant on
the image width: 
$bytes\_ram\_use = (kernel\_size - 1)*image\_width*bytes\_per\_pixel + (kernel\_size - 1)$ and 
The result is the unaltered from if both images were stored.
This is shown in Figure \ref{fig:filtering}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.3\textwidth]{filtering}
    \caption{Sampling the image using a sliding window}
    \label{fig:filtering}
\end{figure}


\subsection{Method}
To reduce the RAM required, a method of processing image data by 
samping each pixel sequentially is proposed. The image is taken, and 
only a single byte is read from the camera. The rest of the image can 
be stored externally to the microcontroller in first-in-first-out 
memory (hardware) or the photo can be re-taken every time a new pixel 
is required. Each pixel is processed independantly of it's neighbours, 
and then discarded. (shown in Figure \ref{fig:perpixel})
\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{perpixel}
    \caption{Locating the centroid on a per-pixel basis}
    \label{fig:perpixel}
\end{figure}

For processing algorithms that do not require multisampling, processing 
without storing the image has no effect. If the algorithm requires 
sampling neary pixels (as in a filtering kernel), then some previous 
pixels need to be stored in a rolling buffer.

\subsection{Gaussian Filtering}
The images coming from a camera are never perfect representations of 
the scene as the camera is influenced by various noise sources.
This can often be modeled as gaussian noise \cite{bib:noise}. When 
calculating the centroid of a coloured are this will cause outliers - 
distorting the calculated centroid.

One method for removing gaussian noise is to use a 
filtering kernel. A filtering kernel adjusts the value of a pixel based 
on the values of surrounding pixels. \cite{bib:gaussiansegmentation} 
estabished that while a gaussian blur reduces the information contained 
in an image, it is useful for generating masks (ie finding blocks 
of similar appearance). Existing implementations of filtering kernals 
are designed to operate as a processing pass, and require the entire 
image to be stored in RAM.

The proposed solution is to do on 
a one dimensional filtering of the image, resulting in the filtering 
only happening on a single axis. This requires only three pixels to be 
stored, and the algorithm is completely resolution independant. The
ram required is
$ bytes\_ram\_use = kernel\_size*bytes\_per\_pixel$
Because the only data available to sample are past data points, 
applying a one-dimension filtering kernel will appear to shift the 
centroid location. This is shown diagramatically in Figure 
\ref{fig:center}. This can to be compensated for when outputting
the centroid by simply subtracting  the kernel width.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.2\textwidth]{center}
    \caption{The offset of a past-data only filter}
    \label{fig:center}
\end{figure}





\subsection{Thresholding}
The system needs to segment the image to locate the area it is
intereseted in. The most commmon method to do so is thresholding 
\cite{bib:threshsold}. This can be done on a per-pixel basis and  to 
reduce the RAM required it will not be stored to a bit-map but be 
passed straight into the centroid finding algorithm. Thresholding is 
performed in a colour space that separates the lighting information 
from the colour information. This has resulted in YUV and YCrCb (which 
separate out a luminance channel\cite{bib:tv}) being used in applications 
such as detecting skin colour for face detection \cite{bib:face}. In 
this case YUV was chosen because as well as removing the imact of 
lighting, it allows two colours (red and blue) to be easily 
distinguished with a single comparison. Other colour spaces would 
require more. For example to locate the colour blue in RGB three 
comparisons need to be made to: low red, low green, high blue. 
Similarly in HSV: high saturation, hue upper bound, hue lower bound. To 
locate any colour range six comparisons must be performed, but if 
saving processing power is critical and there is control over the 
environment and the features to be identified, identifying red and blue 
in YUV offers the lowest computation route. An example of a single 
comparison in YUV space is shown in Figure \ref{fig:threshold}.

Some cameras such as the OV2640, can output their data in YUV colour 
space\cite{bib:ov2640}, as this is type of encoding is used in PAL and 
NTSC video signals\cite{bib:pal}. So thresholding in this colour space 
does not require the microprocessor to perform the colour space 
conversion.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{threshold}
    \caption{Thresholding an image (left) in redness (centre) and blueness (right) }
    \label{fig:threshold}
\end{figure}

\subsection{Locating the Centroid}
Updating the position of the centroid is done by averaging the 
locations of all the pixels that pass the threshold. This is described 
in \cite{bib:handbook} as:
\begin{center}
\begin{eqnarray}
    CG_{x} = \frac{\sum{x_{i}}}{Area}
\end{eqnarray}
\end{center}
If the image is large, then the sum of the values can get extremely
large, possily causing an overflow if a large number of pixels meets the
threshold. The summation equation was re-arranged into a cumulative
moving average using:
\begin{center}
    \begin{eqnarray}
        decay &=& \frac{1}{pixel\_number}\\
        avg &=& new\_value * decay + avg * (1 - decay)
    \end{eqnarray}
\end{center}
In this re-arrangement, the maximum value at any point in the 
computation never exceeds the maximum value sent in for averaging.
However it does require multiple floating point operations.
There have to be two separate averages, one for the vertical location
and one for the horizontal location. 

\section{Implementation Notes}
To avoid lengthy delays in prototyping on an embedded system, the bulk 
of the development was done on a normal linux computer. 
It has been assumed that an existing library for the embedded system 
exists allowing it to access the camera data. As such, the system 
allowing the ram reduction (not storing the photo) was not implemented. 
However the algorithm only samples each pixel once, and is thus 
suitable to be implemented on an embedded system where the image is 
never stored.
To load the test images, scale them and convert the colour space into
YUV, OpenCV was used. 

\section{Results}
\subsection{Test Images}
A series of test images was generated in order to test the algorithm
with a known ground truth. 
These are 40x40px images with several shapes and noise levels. All 
images go between 25\% grey and 75\% grey, with the threshold for 
testing the algorithm set to 50\% grey. The test images are visible in 
Figure \ref{fig:testimages}. The later images have gaussian noise added 
to test the filtering. Image (e) has 33\% amplitude noise and image (f) 
has 66\% amplitude noise.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{testimages}
    \caption{The 6 test images in order of complexity}
    \label{fig:testimages}
\end{figure}

For the noiseless images, the colours were thresholded correctly
and the centroid was calculated correctly.
For image (e) with 33\% noise, 33 pixels were influenced by the noise.
For image (f) with 66\% noise, 372 pixels were influenced by the noise.

Three filtering kernels were tried. They are gaussian blur kernels and 
are visible in Figure \ref{fig:kernels}.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.45\textwidth]{kernels}
    \caption{Three filtering kernels}
    \label{fig:kernels}
\end{figure}

In order to quantify the results, the number of pixels different from
image (d) was counted. The more effective the filtering, the more 
effective the noise removal and the fewer differences from the 
noiseless image.

How many pixels were influenced by the noise, and the percentage of 
total pixels are shown in Table \ref{table:filtering}.

\begin{table}[h]
    \centering
    \begin{tabular}{| c | c | c | c | c | }
      \hline
      Image Source  & Filtering Kernel & Incorrect Pixels & Percentage Incorrect \\
      \hline
      (e)			& None	& 33		& 1.3\% \\
      (e)           & 1x3 Gaussian  & 4           & 0.2\%  \\
      (e)           & 1x5 Gaussian  & 6           & 0.3 \% \\
      (e)           & 3x3 Gaussian  & 4           & 0.2\%  \\
      \hline
      (f)			& None	& 372		& 14.9\% \\
      (f)           & 1x3 Gaussian  & 127           & 5.1\%  \\
      (f)           & 1x5 Gaussian  & 88          & 3.5 \% \\
      (f)           & 3x3 Gaussian  & 29           & 1.2\%  \\
      \hline
    \end{tabular}
    \caption{Number of pixels distorted by noise}
    \label{table:filtering}
\end{table} 
As can be seen, the one dimensional filters did reduce the impact of
noise significantly, but the two dimensional filter was more
effective. This is unsurprising as the number of pixels sampled
by a 3x3 filtering kernel is 9, wheras a 1x5 kernel is only 5. This is 
why the 3x3 gaussian kernel is nearly twice as effective at filtering 
the noise in the two test images.

By displaying the result of thresholding as each pixel is processed, 
the thresholded image can be displayed. A comparison between 1x5 and 
3x3 can be seen in Figure \ref{fig:compare}.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{compare}
    \caption{The results of thresholding for two kernels}
    \label{fig:compare}
\end{figure}
As can be seen, the 1-D gaussian blur fixes most single incorrect 
pixels, but when there are two or three incorrect in a row it is unable
to compensate.

\subsection{Photos}
As well as testing the filtering on test images, several photos were 
processed. The images were filtered using the 3-pixel gaussian blur 
single dimension kernel and the centroids of red and blue areas exceed 
60\% (independantly) was found.
To reduce the ram required, each of these images was processed twice:
once for the red area and a second time for the blue area. In a 
real-world environment this would have the possibility of the camera or 
target moving.
The images were processed at 600x360px resolution. 
Some of the test images are shown with the detected centroids indicated 
by the red and cyan croshairs. The lines were not drawn in by the 
program, they were drawn afterwards using the centroid location output 
by the algorithm.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.45\textwidth]{Images/test1.jpg}
    \caption{A test photo under a normal situation}
    \label{fig:testphoto1}
\end{figure}

Figure \ref{fig:testphoto1} shows a normal situation, with clearly
defined red and blue areas. Shading is present on the red sphere, but
the choice of colour space (YUV) has resulted in the entire sphere
being corretly detected. This is also shown in Figure 
\ref{fig:testphoto2}, where the shadow of the photographers hand
has no influence on the correctness of the detection.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.45\textwidth]{Images/test2.jpg}
    \caption{A test photo with shadows}
    \label{fig:testphoto2}
\end{figure}

Figure \ref{fig:testphoto4} has significantly smaller coloured areas,
but it was still detected. Note that the red center is offset. This
is due to the presence of the photographers finger in the top
right of the image. This is a limitation of the current implementation:
it can only detect a single coloured blob. If multiple identicle 
coloured blobs are present, only a single centroid is calculated which 
will be located between them.

A more cluttered environment was tested in Figure \ref{fig:testphoto5}.
and the algorithm still performed well.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.45\textwidth]{Images/test4.jpg}
    \caption{A test photo where the object is small. Note the finger in
    the top right causing the offset on the red channel}
    \label{fig:testphoto4}
\end{figure}


\begin{figure}[h]
    \centering
    \includegraphics[width=0.45\textwidth]{Images/test5.jpg}
    \caption{A cluttered test environment}
    \label{fig:testphoto5}
\end{figure}

\subsection{Ram Usage}
It is useful to identify where the ram is being used in this process.
For this to be done, the program was analysed line by line and
the total quantity of data required was estimated based on operation.

For example the line:
\begin{lstlisting}
*Cr = (Cr1/KERNEL_1 + Cr2/KERNEL_2 + Cr3/KERNEL_3); 
\end{lstlisting}

can be expanded to:
\begin{lstlisting}
uint8_t Cr1
uint8_t Cr2
uint8_t Cr3
uint8_t KERNEL_1 = 4;
uint8_t KERNEL_2 = 2;
uint8_t KERNEL_3 = 4;

uint8_t val1 = Cr1 / KERNEL_1;
uint8_t val2 = Cr2 / KERNEL_2;
uint8_t val3 = Cr3 / KERNEL_3;
uint8_t val4 = val1 + val2;
uint8_t val5 = val4 + val3;
*Cr = val5;
\end{lstlisting}
There are a total of 11 uint8\_t datatypes required, and thus computing 
that line of code used a total of 11 bytes of data.

Using the analysis technique described above, the total data required
for each section of the algorithm was tabulated into Table 
\ref{tab:stored}.


\begin{table}[h]
    \centering
    \begin{tabular}{| p{2cm} | p{1.4cm} | p{4cm} | }
      \hline
      Section  & Data Required & Description \\
      \hline
      Reading pixel &  -   & Dependant on camera\\
      Thresholding  & 9 bytes   & Upper and lower bounds for three colour channels\\
      3-px filter   & 33 bytes & Weighted average of three pixels for three channels\\
      5-px filter   & 60 bytes & Weighted average of five pixels for three channels\\
      Average Centroid & 52 bytes & Cumulative moving average\\
	  Overhead 	& 11 bytes & Keeping track of location in image and storing current pixel value\\
      \hline
      
    \end{tabular}
    \caption{Data stored by processing section}
    \label{tab:stored}
\end{table} 

A naive approach would suggest that the algorithm requires 105-113 bytes
of ram to run, but this is an inaccurate conclusion as function calls
require overhead and the C compiler will re-use the stack as the 
functions return. To identify the exact ram required, disassembly of 
the binary would be required and it would likely depend on the compiler 
used and it's optimization settings. Thus this value represents the 
number of bytes assuming static memory allocation. 

What can be drawn from this data is where the data is used. One of the
significant users of data in this breakdown is averaging the centroid
location. This is because it performs a lot of floating point
operations (and a float is 32 bits/4 bytes long). This could no doubt
be reduced if the averaging could be run using integer arithmetic.


\section{Conclusion}
The method presented here is capable of locating the centroid of
coloured areas using 113 bytes of data. While the exact ram use
will depend on the compiler used, the overhead caused by 
function calls and the method used to retrieve data from camera,
a comparison can be made between the method presented here and
a more traditional approach is to compute the data that would be 
required to store the image. The test images were 40x40px, and contain 
40*40*3 bytes of data (4.8kB). The photos were 600x360px, and contain 
648kB of data. Thus is can be concluded that even at low resolutions, 
processing an image on a per-pixel basis can save several orders of 
magnitude of storage space.

Filtering the image before thresholding was successful, but not as
effective as a normal gaussian blur. While a 5px single dimension
blur reduced the pixels affected by noise in the test images from 1.3\% 
to 0.3\%, and from 14.9\% to 3.5\% a more typical 3x3 gaussian blur 
reduced the noise from 1.3\% to 0.2\% and from 14.9\% to 1/2\%.

%At some point, the image must be stored - be it in the microprocessors
%memory, an external FIFO, or within the camera module. The advantage of
%storing the image externally is that it does not occupy resources that 
%my be used by other processes. Even if it is decided to store the image
%%in ram, it is likely that processing a pixel in it's entirety 
%can save RAM as it means only a single copy of the image needs to be 
%stored - not multiple passes as in some algorithms.

The procedure of processing an image one pixel at a time relies on
the microcontroller to be fast enough to process each pixel as it
comes in from the camera. For a serial camera it must be capable of 
processing a pixel in the time it takes to transmit a the next one. 
For a the uCAM-II\cite{bib:ucam}, capable of operating between 9600
and 115200 baud, and sending 16-bit-per-pixel, each pixel must
be processed in $1/600^{th} - 1/7200^{th}$ of a second. In 2016,
most microcontrollers operate in the Mhz region, and with optiization, 
should be capable of processing pixels at above 600Hz.


\subsection{Further Research}
The immediate next step is to prototype this algorithm on an 
embedded system and evaluate it's CPU performance. While significant 
savings in RAM usage have been made, it is likely the CPU usage has
increased and on the low-spec hardware this method is designed
for, the resulting performance may be poor.

Locating the centroid of coloured areas is a fundamental building
block of computer vision. As such it can be developed in many ways.
One of these is to develop methods of locating separate coloured blobs 
in the same image. This could potentially be achieved by setting a 
maximum difference between the centroid and the current pixel 
location. It could be scaled by the known area of the centroid so
that large blobs are still detected as singular.

Other possible extensions are ambient light removal, which could be done
by averaging the colour of the image, and subtracting this colour 
from the sampled image.

If three coloured blocks are detected able to be detected, a 6DOF pose 
estimate can be established. This can be implemented with little 
modification, requiring the addition of concurrently detecting 
different colour blocks within the same frame. It is likely this could 
be performed in less than 2kB of ram.


 \subsection{Potential Applications}
The intended application for such a system is to allow a robot
to detect it's home base (a blue area) from a distance and 
adjust it's course towards it. In it's current form the algorithm is 
certainly capable of filling this function. It is also capable of 
performing any task that is done through tracking a single object - 
such as tracking the motion of a coloured object through a room.
The algorithm could also be used to pre-process an image. If it's 
thresholds are set to detect human skin colour it could be used to 
identify where in a frame further processing must be performed. As a 
low resource pre-process it does not require high clock speeds, meaning 
that the processor can spend more of it's time in a power-saving state 
or performing other tasks.


 

\newpage
\begin{thebibliography}{1}

\bibitem{bib:sense}
Ruocco, S. R. (1987). Robot sensors and transducers. New York;Milton Keyes, England;: Halsted Press.

\bibitem{bib:blob1}
Wang, Y., \& De Silva, C. W. (2006). a fast and robust algorithm for color-blob tracking in multi-robot coordinated tasks. International Journal of Information Acquisition, 3(3), 191-200. doi:10.1142/S0219878906000952

\bibitem{bib:blob2}
Argyros, A. A., \& Lourakis, M. I. A. (2005). Tracking multiple colored blobs with a moving camera. Paper presented at the , 2 1178 vol. 2. doi:10.1109/CVPR.2005.348

\bibitem{bib:arduino}
Atmel, Atmel ATmega640/V-1280/V-1281/V-2560/V-2561/V. Datasheet, 2014.

\bibitem{bib:atmel}
Atmel, SAM3S8 / SAM3SD8. Datasheet, 2014.

\bibitem{bib:sliding}
Benda, Lukas. Hardware Acceleration for Image Processing. Ecole Polytechnique Federale de Lausanne

\bibitem{bib:noise}
Roudot, P., Kervrann, C., Boulanger, J., \& Waharte, F. (2013). Noise modeling for intensified camera in fluorescence imaging: Application to image denoising. Paper presented at the 600-603. doi:10.1109/ISBI.2013.6556546

\bibitem{bib:handbook}
Russ, John C. The Image Processing Handbook. Boca Raton, FL: CRC Press, 2011. Print.

\bibitem{bib:gaussiansegmentation}
  Estevão S. Gedraite, Murielle Hadad. Investigation on the Effect of a Gaussian Blur in
Image Filtering and Segmentation. University of São Paulo, 2011.

\bibitem{bib:threshsold}
Parker, J. R. Algorithms for Image Processing and Computer Vision. Wiley 2010. pp. 137

\bibitem{bib:tv}
W. Wharton \& D. Howorth. Principles of Television Reception. Pitman Publishing, 1971, pp 161-163

\bibitem{bib:face}
Kewei, S., Xitian, F., Anni, C., \& Jingao, S. (1999). Automatic face segmentation in YCrCb images. Paper presented at the , 2 916-919 vol.2. doi:10.1109/APCC.1999.820412

\bibitem{bib:ov2640}
Omnivision, OV2640 Color CMOS UXGA (2.0 MegaPixel) Camera Chip. Datasheet, Feb 2006.

\bibitem{bib:pal} 
Jack, Keith. Video Demystified: A Handbook For The 
Digital Engineer. Newnes, 2007. Print.

\bibitem{bib:ucam}
4D Systems, Serial Camera Module uCAM-II. Datasheet, 2014

\end{thebibliography}

% that's all folks
\end{document}


