// display the difference between adjacent video frames (press any key to exit)

#define IMAGE_PATH "../test-images/test%d.png"

#include <stdio.h>
#include <stdlib.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>



using namespace cv;
using namespace std;
vector<Mat> spl;
Mat full;
Mat image;
VideoCapture capture(0); // connect to the webcam
Size2i size(50,50); 


 int imageSource = 0;

char filename[100] = {0};
int frameNumber = 0;

int pixelNum = 0;

void getPixel(uint8_t* Y, uint8_t* Cr, uint8_t* Cb){
    //Note that pixels start in top left of the image
    
    int current_x = pixelNum % size.width;
    int current_y = pixelNum / size.width;
    pixelNum += 1;
    Vec3b color = image.at<Vec3b>(Point(current_x,current_y));
    
    *Y = color.val[0];
    *Cr = color.val[1];
    *Cb = color.val[2];
}

void setSource(int src){
    imageSource = src;
}


int takeImage() {
    using namespace std;
    frameNumber += 1;
    if (imageSource == 0){
        capture.read(full); // get the first frame (or use capture >> image;)    
        cvtColor(full, full, cv::COLOR_BGR2YUV);
        resize(full, image, size);
        
    } else {
        sprintf(filename, IMAGE_PATH, frameNumber);
        printf("File: %s\n", filename);
        full = imread(filename, CV_LOAD_IMAGE_COLOR);
        cvtColor(full, full, cv::COLOR_BGR2YUV);
        resize(full, image, size);
    }
    pixelNum = 0;
    imshow("Y", full & Scalar(0,255,0));
    //imshow("Cr", full & Scalar(0,255,0));
    //imshow("Cb", full & Scalar(0,0,255));
    //printf("The errant Pixel: %d", image.at<Vec3b>(Point(49,49)).val[0]);
    //printf("The errant Pixel: %d", image.at<Vec3b>(Point(49,49)).val[0]);
    waitKey(1);
}



