#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

#include "camera.hpp"

//Program constants:
#define IMAGE_SIZE_X 50
#define IMAGE_SIZE_Y 50

//To calculate centroid the following are required:
float centroid_x = 0;
float centroid_y = 0;
uint16_t current_x = 0;
uint16_t current_y = 0;
uint32_t centroid_area = 0; //Should be able to contain size_x * size_y

//To do a pre blur filter the folliwing are required:
#define KERNEL_1 1/4  // 1/kernel_1 + 1/kernel_2 + 1/kernel_3 should add to 1
#define KERNEL_2 1/2
#define KERNEL_3 1/4
uint8_t Y1 = 0, Cr1 = 0, Cb1 = 0;
uint8_t Y2 = 0, Cr2 = 0, Cb2 = 0;
uint8_t Y3 = 0, Cr3 = 0, Cb3 = 0;

//To do a post blur filter the following are required
bool P1 = 0, P2 = 0, P3 = 0;

void clearState(){
    /* Clears the internal state of the CV system */
    centroid_x = 0;
    centroid_y = 0;
    current_x = 0;
    current_y = 0;
    centroid_area = 0;
    
    Y1 = 127;
    Y2 = 127;
    Y3 = 127;
    Cr1 = 127;
    Cr2 = 127;
    Cr3 = 127;
    Cb1 = 127;
    Cb2 = 127;
    Cb2 = 127;
    
    P1 = 0;
    P2 = 0;
    P3 = 0;
}

void average_pixel(int x_pos, int y_pos, int area_so_far){
    centroid_x = (x_pos + centroid_x * (area_so_far - 1))/ (area_so_far);
    centroid_y = (y_pos + centroid_y * (area_so_far - 1))/ (area_so_far);
    
    //float alpha = 1.0/area_so_far;
    //centroid_x = ((x_pos * alpha) + (centroid_x * (1 - alpha)));
    //centroid_y = ((y_pos * alpha) + (centroid_y * (1 - alpha)));
}

bool post_blur_filter(bool pixel){
    uint8_t sum = 0;
    P3 = P2;
    P2 = P1;
    P1 = pixel;
    
    sum = P1 + P2 + P3;
    if (sum > 1){
        return 1;
    } else {
        return 0;
    }
}

void pre_blur_filter(uint8_t* Y, uint8_t* Cr, uint8_t* Cb){
    //Move all data along by one
    Y3 = Y2;
    Cr3 = Cr2;
    Cb3 = Cb2;
    
    Cr2 = Cr1;
    Cb2 = Cb1;
    Y2 = Y1;
    
    Cb1 = *Cb;
    Cr1 = *Cr;
    Y1 = *Y;
    
    //Do an average
    *Y = (Y1*KERNEL_1 + Y2*KERNEL_2 + Y3*KERNEL_3);
    *Cr = (Cr1*KERNEL_1 + Cr2*KERNEL_2 + Cr3*KERNEL_3); 
    *Cb = (Cb1*KERNEL_1 + Cb2*KERNEL_2 + Cb3*KERNEL_3);
}


int threshold(uint8_t Y, uint8_t Cr, uint8_t Cb){
    //pre_blur_filter(&Y, &Cr, &Cb);
    //if (Cr > 153){
    if (Y <= 128){
        return 1;
        //return post_blur_filter(1);
    } else {
        return 0;
        //return post_blur_filter(0);
    }
}


int testCentroiding(){
    uint16_t area = 0;
    while (1){
        printf("Enter X and Y pixels that meet the threshold separated by a comma: ");
        int x = 0;
        int y = 0;
        scanf("%d, %d", &x, &y);
        area += 1;
        average_pixel(x, y, area);
        
        printf("The centroid so far is %f, %f\n", centroid_x, centroid_y);
    }
}


int main(){
    uint8_t Y = 0;
    uint8_t Cr = 0;
    uint8_t Cb = 0;
    
    printf("Select Image Source: 0 for webcam, 1 for test images: ");
    int img_src = 0;
    scanf("%d", &img_src);
    setSource(img_src);
    
    //testCentroiding();
    while(1){
        clearState();
        takeImage();
        for (current_y = 0; current_y < IMAGE_SIZE_Y; current_y++){
            for (current_x = 0; current_x < IMAGE_SIZE_X; current_x++){
                getPixel(&Y, &Cr, &Cb);
                if (threshold(Y,Cr,Cb) == 1){
                    printf("[]");
                    centroid_area += 1;
                    average_pixel(current_x+1, current_y+1, centroid_area);
                } else {
                    printf("  ");
                }
            }
            printf("\n");
        }
        printf("The centroid is: (%f, %f)\n", centroid_x, centroid_y);
    }
    
}


